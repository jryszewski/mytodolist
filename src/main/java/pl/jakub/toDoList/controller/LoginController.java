package pl.jakub.toDoList.controller;

import pl.jakub.toDoList.dao.LoginDao;
import pl.jakub.toDoList.model.LoginBean;
import pl.jakub.toDoList.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private LoginDao loginDao;

    public void init() {
        loginDao = new LoginDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        authenticate(request, response);
    }

    private void authenticate(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        LoginBean loginBean = new LoginBean();
        loginBean.setUsername(username);
        loginBean.setPassword(password);
        User user = new User();

// zrobic zapytanei do bazy o id uzytkowniak na podstawie nazwy uzytkownika
       // String usernameID = "select id from users where username = ?";

        try {

            user = loginDao.validate(loginBean);
            //wynik metody validate do obiektu user jesli user == null tzn ze
            if (user!=null) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("todo-list.jsp");
                dispatcher.forward(request, response);
                HttpSession session = request.getSession();
                session.setAttribute("user", user.getId());
                session.setAttribute("user", username);


            } else {
                HttpSession session = request.getSession();
                session.setAttribute("user", username);
                response.sendRedirect("login.jsp");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
//zapytanie o id usera
//
// request session przenies wyzej do ifa

// przy logowaniu sprawdzic credentials czy uzywtkownik podal login i haslo jesli
// tak to w sesji zapisac jego ID, jesli zapisze w sesji jego ID to potem przy kazdym kolejnym reqescie
// od uzytkownika bede ostawal obiekt tej sejsji, jesli yzytkownik bedzie chcial dodac item to pobieram jego id
// irobie qery do bazy danych z jego ID,