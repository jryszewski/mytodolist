package pl.jakub.toDoList.dao;

import pl.jakub.toDoList.model.LoginBean;
import pl.jakub.toDoList.model.User;
import pl.jakub.toDoList.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDao {

    public User validate(LoginBean loginBean) throws ClassNotFoundException {
        boolean status = false;
        User user = new User();

        Class.forName("com.mysql.jdbc.Driver");

        try (Connection connection = JDBCUtils.getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection
                     .prepareStatement("select * from users where username = ? and password = ? ")) {
            preparedStatement.setString(1, loginBean.getUsername());
            preparedStatement.setString(2, loginBean.getPassword());

            //wyciagnac z funkcji validate nazwe uzytkownika z resultsetu
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            user.setId(rs.getInt("id"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));

            // przy logowaniu sprawdzic credentials czy uzywtkownik podal login i haslo jesli
            // tak to w sesji zapisac jego ID, jesli zapisze w sesji jego ID to potem przy kazdym kolejnym reqescie
            // od uzytkownika bede ostawal obiekt tej sejsji, jesli yzytkownik bedzie chcial dodac item to pobieram jego id
            // irobie qery do bazy danych z jego ID,


        } catch (SQLException e) {
            // process sql exception
            JDBCUtils.printSQLException(e);
        }
        return user;
    }
}
