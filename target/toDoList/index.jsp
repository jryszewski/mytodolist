<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>Insert title here</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

<jsp:include page="header.jsp"></jsp:include>
<div class="container col-md-8 col-md-offset-3" style="overflow: auto">
    <h1>Welcome to your To Do List !</h1>
    <h3>Let's start with: </h3>
    <li><a href="register" class="nav-link">Signup</a></li>
    <h3>If your already have account:  </h3>
    <li><a href="login" class="nav-link">Login</a></li>
    </form>
</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>

</html>